<?php

namespace Gamesmkt\FishpondRecord\DisplayData;

use Gamesmkt\FishpondRecord\DisplayDataInterface;

interface MultilingualInterface
{
    public function translatorLoad(DisplayDataInterface $displayData);
}
